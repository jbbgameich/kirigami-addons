#include <QQmlExtensionPlugin>
#include <QQmlEngine>

#include "dirmodel.h"
#include "dirmodelutils.h"

class KirigamiAddonsFilePickerPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    KirigamiAddonsFilePickerPlugin() = default;
    ~KirigamiAddonsFilePickerPlugin() override = default;
    void initializeEngine(QQmlEngine *engine, const char *uri) override {
        Q_UNUSED(engine)
        Q_UNUSED(uri)
    }
    void registerTypes(const char *uri) override;
};

void KirigamiAddonsFilePickerPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<DirModel>(uri, 0, 1, "DirModel");
    qmlRegisterType<DirModelUtils>(uri, 0, 1, "DirModelUtils");
}

#include "plugin.moc"
