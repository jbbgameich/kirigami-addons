import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.2 as QQC2
import org.kde.kirigami 2.5 as Kirigami
import org.kde.kirigamiaddons.filepicker 0.1 as Addon

Kirigami.ApplicationWindow {
    id: test
    property bool selectMultiple: false
    property bool selectExisting: false
    property bool selectFolder: true

    title: "File Picker Test"
    pageStack.initialPage: Kirigami.Page {
        title: "Test Page"
        Addon.FilePicker {
            id: filePicker
            selectMultiple: test.selectMultiple
            selectExisting: test.selectExisting
            selectFolder: test.selectFolder
        }
        Kirigami.FormLayout {
            anchors.fill: parent
            QQC2.CheckBox {
                Kirigami.FormData.label: "is mobile"
                checked: Kirigami.Settings.isMobile
                enabled: false
            }
            QQC2.Switch {
                id: folderSwitch
                Kirigami.FormData.label: "select folder"
                checked: test.selectFolder
            }
            QQC2.Switch {
                id: existingSwitch
                Kirigami.FormData.label: "select existing"
                checked: test.selectExisting
            }
            QQC2.Switch {
                id: multipleSwitch
                Kirigami.FormData.label: "select multiple"
                checked: test.selectMultiple
            }
            QQC2.Button {
                Kirigami.FormData.label: "File Picker"
                text: "Open File Picker"
                onClicked: filePicker.open()
            }
            QQC2.Label {
                Kirigami.FormData.label: "result"
                text: test.selectMultiple ? JSON.stringify(filePicker.fileUrls) : filePicker.fileUrl
            }
        }
    }
}
