import QtQuick 2.7
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.5 as Kirigami

import org.kde.kirigamiaddons.filepicker 0.1

Item {
    id: root

    property string folder
    property url fileUrl
    property var fileUrls: []
    property string title
    signal accepted
    property var nameFilters: [] // Not (yet) implemented
    property bool selectMultiple: false
    property bool selectExisting: true
    property bool selectFolder: false

    Component {
        id: fileChooserPage

        Kirigami.ScrollablePage {
            title: root.title
            header: Controls.ToolBar {
                Row {
                    Controls.ToolButton {
                        icon.name: "folder-root-symbolic"
                        height: parent.height
                        width: height
                        onClicked: dirModel.folder = "file:///"
                    }

                    Repeater {
                        model: utils.getUrlParts(dirModel.folder)

                        Controls.ToolButton {
                            icon.name: "arrow-right"
                            text: modelData
                            onClicked: dirModel.folder = utils.indexOfUrl(
                                           dirModel.folder, index)
                        }
                    }
                }
            }
            footer: RowLayout {
                visible: !root.selectExisting
                height: root.selectExisting ? 0 : Kirigami.Units.gridUnit * 2
                Controls.TextField {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    id: fileNameField
                    placeholderText: i18n("File name")
                }
                Controls.ToolButton {
                    Layout.fillHeight: true
                    icon.name: "dialog-ok-apply"
                    onClicked: {
                        root.fileUrl = dirModel.folder + "/" + fileNameField.text
                        root.accepted()
                    }
                }
            }

            mainAction: Kirigami.Action {
                visible: (root.selectMultiple || root.selectFolder) && root.selectExisting
                text: i18n("Select")
                icon.name: "object-select-symbolic"

                onTriggered: root.accepted()
            }

            DirModel {
                id: dirModel
                folder: root.folder
                showDotFiles: false
                onFolderChanged: {
                    console.log(dirModel.folder)
                }
            }

            DirModelUtils {
                id: utils
            }

            Controls.BusyIndicator {
                anchors.centerIn: parent

                width: Kirigami.Units.gridUnit * 4
                height: width

                visible: dirModel.isLoading
            }

            ListView {
                anchors.fill: parent
                model: dirModel
                clip: true

                delegate: Kirigami.BasicListItem {
                    text: model.name
                    icon: model.iconName
                    checkable: root.selectMultiple

                    onClicked: {
                        if (model.isDir) {
                            if (root.selectFolder)
                                root.fileUrl = model.url

                            dirModel.folder = model.url
                        } else {
                            if (root.selectMultiple) {
                                root.fileUrls.push(model.url)
                            } else {
                                root.fileUrl = model.url
                                root.accepted()
                            }
                        }
                    }
                }
            }
        }
    }

    onAccepted: pageStack.pop()

    function open() {
        // Reset fileUrls
        if (selectMultiple)
            root.fileUrls = []

        pageStack.push(fileChooserPage)
    }
}
