#ifndef DIRMODELUTILS_H
#define DIRMODELUTILS_H

#include <QObject>

class DirModelUtils : public QObject
{
	Q_OBJECT
    Q_PROPERTY(QString homePath READ homePath NOTIFY homePathChanged)

public:
    explicit DirModelUtils(QObject *parent = nullptr);

public Q_SLOTS:
    QStringList getUrlParts(const QUrl &url) const;
    QUrl indexOfUrl(const QUrl &url, int index) const;
    QString homePath() const;

Q_SIGNALS:
    void homePathChanged();
};

#endif // DIRMODELUTILS_H
